# frozen_string_literal: true

module App
  class AccountsController < BaseController
    def index
      @accounts = Current.user.accounts
    end

    def show
      @account = Current.user.accounts.find(params[:id])
      Current.account = @account
      cookies.signed[:account_id] = @account.id
      @projects = @account.projects
    end

    def new
      @account = Account.new
    end

    def edit
      @account = Account.find(params[:id])
    end

    def create
      @account = Services::Accounts::Create.call(account_params)

      respond_with :app, @account
    end

    def update
      @account = Account.find(params[:id])
      @account.update(account_params)

      respond_with :app, @account
    end

    def page_title
      I18n.t(action_name, scope: [:page_title, controller_name], resource_name: @account&.name)
    end

    protected

    def account_params
      params.require(:account).permit(:name)
    end

    def current_navbar_item
      :accounts
    end
  end
end
