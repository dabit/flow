class AddWorkItemStatusIdToWorkItems < ActiveRecord::Migration[7.0]
  def change
    change_table :work_items do |t|
      t.belongs_to :work_item_status, null: false, foreign_key: true, type: :uuid
    end
  end
end
