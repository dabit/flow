# frozen_string_literal: true

module Services
  module Accounts
    class Create < ApplicationService
      def call(account_params)
        Account.new(account_params).tap do |account|
          account.account_users.create(user: Current.user, role: 'owner') if account.save
        end
      end
    end
  end
end
