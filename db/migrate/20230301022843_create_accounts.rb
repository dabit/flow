class CreateAccounts < ActiveRecord::Migration[7.0]
  def change
    create_table :accounts, id: :uuid do |t|
      t.string :name, null: false
      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
