# frozen_string_literal: true

# == Schema Information
#
# Table name: features
#
#  id         :uuid             not null, primary key
#  deleted_at :datetime
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  project_id :uuid             not null
#  user_id    :uuid             not null
#
# Indexes
#
#  index_features_on_deleted_at  (deleted_at)
#  index_features_on_project_id  (project_id)
#  index_features_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (user_id => users.id)
#
require 'test_helper'

class FeatureTest < ActiveSupport::TestCase
  let(:feature) { create(:feature) }

  describe 'work_items_total' do
    before { create_list(:work_item, 3, :backlog, feature:) }

    it 'returns the number of work items' do
      _(feature.work_items_total).must_equal 3
    end
  end

  describe 'work_items_done' do
    before do
      create_list(:work_item, 3, :backlog, feature:)
      create_list(:work_item, 2, :done, feature:)
    end

    it 'returns the number of work items done' do
      _(feature.reload.work_items_done).must_equal 2
    end
  end

  describe 'progress' do
    describe 'when featurea has stories' do
      before do
        create_list(:work_item, 8, :backlog, feature:)
        create_list(:work_item, 2, :done, feature:)
      end

      it 'returns the progress of the feature' do
        _(feature.reload.progress).must_equal 20.0
      end
    end

    describe 'when feature has no stories' do
      it 'returns 0' do
        _(feature.reload.progress).must_equal 0.0
      end
    end
  end
end
