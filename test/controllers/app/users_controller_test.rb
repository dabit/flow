# frozen_string_literal: true

require 'test_helper'

module App
  class UsersControllerTest < ActionDispatch::IntegrationTest
    let(:account) { create(:account) }
    let(:user) { account.owner }

    before do
      sign_in user
      get app_account_path(account)
    end

    describe 'POST #create' do
      it 'redirects to users_path' do
        post app_users_path, params: {
          user: {
            email: FFaker::Internet.email
          }
        }

        assert_redirected_to app_users_path
      end
    end

    describe 'GET #index' do
      before { create(:user, :with_profile, with_account: account) }

      it 'is successful' do
        get app_users_path

        assert_response :success
      end
    end

    describe 'GET #new' do
      it 'is successful' do
        get new_app_user_path

        assert_response :success
      end
    end
  end
end
