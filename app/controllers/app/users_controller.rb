# frozen_string_literal: true

module App
  class UsersController < App::BaseController
    before_action :check_current_account

    def index
      @users = Current.account.users.all
      authorize(@users)
    end

    def show; end

    def new
      @user = User.new
    end

    def edit; end

    def create
      @user = Services::Users::Invite.call(user_params[:email], Current.account)

      respond_with :app, @user, location: -> { app_users_path }, notice: t('flash.app.users.create.notice', email: @user.email)
    end

    private

    def current_navbar_item
      :users
    end

    def page_title
      t(action_name, scope: 'page_title.users')
    end

    def user_params
      params.require(:user).permit(:email)
    end
  end
end
