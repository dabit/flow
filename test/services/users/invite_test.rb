# frozen_string_literal: true

require 'test_helper'

module Services
  module Users
    class InviteTest < ActiveSupport::TestCase
      let(:account) { create(:account) }
      let(:user) { account.owner }
      let(:email) { FFaker::Internet.email }

      before { Current.user = user }

      describe 'call' do
        before do
          @user = Services::Users::Invite.call(email, account)
        end

        it 'creates a user with provided email' do
          _(@user).must_be :persisted?
          _(@user.email).must_equal email
        end

        it 'assigns the user to the account' do
          _(@user.accounts).must_include account
        end

        it 'sends an invitation' do
          last_email = ActionMailer::Base.deliveries.last
          _(last_email.to).must_include email
        end
      end

      describe 'call when user already exists' do
        before do
          existing_user = create(:user, :invited, email:)
          existing_user.account_users.create(account:, role: :member)
          @user = Services::Users::Invite.call(email, account)
        end

        it 'sends an invitation' do
          last_email = ActionMailer::Base.deliveries.last
          _(last_email.to).must_include email
        end
      end
    end
  end
end
