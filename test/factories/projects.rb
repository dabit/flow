# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id            :uuid             not null, primary key
#  deleted_at    :datetime
#  identifier    :string           not null
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  account_id    :uuid
#  created_by_id :uuid
#
# Indexes
#
#  index_projects_on_account_id                 (account_id)
#  index_projects_on_created_by_id              (created_by_id)
#  index_projects_on_deleted_at                 (deleted_at)
#  index_projects_on_identifier_and_account_id  (identifier,account_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (created_by_id => users.id)
#
FactoryBot.define do
  factory :project do
    account
    created_by { create :user }
    name { FFaker::Book.title }
    description { FFaker::Lorem.paragraph }
    sequence :identifier do |n|
      "TE#{n}"
    end

    transient do
      work_item_count { 0 }
    end

    after(:create) do |project, evaluator|
      create_list(:work_item, evaluator.work_item_count, project:)
    end
  end
end
