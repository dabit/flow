class CreateWorkItemData < ActiveRecord::Migration[7.0]
  def change
    create_table :work_item_data, id: :uuid do |t|
      t.belongs_to :work_item, null: false, foreign_key: true, type: :uuid
      t.belongs_to :project, null: false, foreign_key: true, type: :uuid
      t.integer :time_to_market_hours
      t.integer :time_to_market_minutes

      t.timestamps
    end
  end
end
