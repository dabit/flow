module ApplicationHelper
  def navbar_active_style(navbar_item, current_navbar_item)
    current_navbar_item == navbar_item ? 'border-primary-500 text-gray-900' : 'border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700'
  end

  def mobile_active_style(navbar_item, current_navbar_item)
    current_navbar_item == navbar_item ? 'border-primary-500 bg-primary-50 text-primary-700' : 'border-transparent text-gray-500 hover:border-gray-300 hover:bg-gray-50 hover:text-gray-800'
  end

  def work_item_type_color(work_item_type)
   case work_item_type
    when 'story'
      'bg-emerald-200 text-emerald-800'
    when 'task'
      'bg-sky-200 text-sky-800'
    when 'bug'
      'bg-red-100 text-red-800'
    end
  end

  def work_item_status_color(work_item_status)
    case work_item_status
    when 'backlog'
      'bg-gray-100 text-gray-800'
    when 'grooming'
      'bg-purple-100 text-purple-800'
    when 'ready_for_dev'
      'bg-indigo-100 text-indigo-800'
    when 'in_progress'
      'bg-blue-100 text-blue-800'
    when 'done'
      'bg-green-100 text-green-800'
    when 'review'
      'bg-yellow-100 text-yellow-800'
    when 'withdrawn'
      'bg-pink-50 text-pink-800'
    when 'fridge'
      'bg-sky-100 text-sky-800'
    end
  end

  def input_error_class(resource, attribute)
    resource.errors[attribute].present? ? 'border-red-300 placeholder-red-200 focus:outline-none focus:ring-red-500 focus:border-red-500' : ''
  end
end
