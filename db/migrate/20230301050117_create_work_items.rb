class CreateWorkItems < ActiveRecord::Migration[7.0]
  def change
    create_table :work_items, id: :uuid do |t|
      t.belongs_to :project, null: false, foreign_key: true, type: :uuid
      t.belongs_to :created_by, foreign_key: { to_table: :users }, null: false, type: :uuid
      t.belongs_to :assigned_to, foreign_key: { to_table: :users }, type: :uuid
      t.integer :work_item_type, null: false
      t.string :name

      t.timestamps
    end
  end
end
