# frozen_string_literal: true

module Services
  module Comments
    class Create < ApplicationService
      def call(commentable, comment_params)
        Comment.create(commentable:, user: Current.user, message: comment_params[:message])
      end
    end
  end
end
