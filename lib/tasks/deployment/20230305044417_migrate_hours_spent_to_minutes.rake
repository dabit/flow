namespace :after_party do
  desc 'Deployment task: migrate_hours_spent_to_minutes'
  task migrate_hours_spent_to_minutes: :environment do
    puts "Running deploy task 'migrate_hours_spent_to_minutes'"

    WorkItem.transaction do
      WorkItem.all.each do |work_item|
        events = work_item.work_item_status_events.order(:created_at)
        events.each_with_index do |event, index|
          next if event.start?

          previous_event = events[index - 1]

          minutes_spent = ((event.created_at - previous_event.created_at) / 1.minute).ceil
          event.update(minutes_spent: minutes_spent)
        end
      end
    end

    # Put your task implementation HERE.

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord
      .create version: AfterParty::TaskRecorder.new(__FILE__).timestamp
  end
end
