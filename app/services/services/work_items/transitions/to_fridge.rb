# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToFridge < Services::WorkItems::Transitions::BaseTransition
        def attributes
          super.merge({ stashed_in_fridge_at: Time.current })
        end
      end
    end
  end
end
