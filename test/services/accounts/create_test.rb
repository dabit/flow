# frozen_string_literal: true

require 'test_helper'

module Services
  module Accounts
    class CreateTest < ActiveSupport::TestCase
      describe 'call' do
        before { Current.user = create(:user) }

        let(:account) { Services::Accounts::Create.call(name: FFaker::Company.name) }

        it 'returns a persisted account' do
          _(account).must_be :persisted?
        end

        it 'set the current user as the owner' do
          _(account.users.count).must_equal 1
          _(account.owner).must_equal(Current.user)
        end
      end
    end
  end
end
