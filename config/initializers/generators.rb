# frozen_string_literal: true

Rails.application.config.generators do |g|
  g.orm :active_record, primary_key_type: :uuid
  g.system_tests = nil
  g.stylesheets false
  g.helper false
  g.jbuilder false
end
