# frozen_string_literal: true

return if Rails.env.production?

I18n::Debug.logger = Logger.new('log/i18n.log')
