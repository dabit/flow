# frozen_string_literal: true

class Current < ActiveSupport::CurrentAttributes
  attribute :user
  attribute :account
  attribute :profile
  attribute :time_zone

  def role
    AccountUser.find_by(account:, user:)
  end

  def user=(user)
    super
    return unless user

    self.profile = user.profile
    self.time_zone = user.profile&.time_zone || 'Central Time (US & Canada)'
  end
end
