# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToBacklog < Services::WorkItems::Transitions::BaseTransition
      end
    end
  end
end
