# frozen_string_literal: true

require 'test_helper'

module Services
  module Projects
    class CreateTest < ActiveSupport::TestCase
      describe 'call' do
        let(:account) { create :account }
        let(:user) { account.owner }
        let(:project) do
          Services::Projects::Create.call(
            {
              name: FFaker::Book.title,
              identifier: 'TEST'
            }
          )
        end

        before do
          Current.user = user
          Current.account = account
        end

        it 'returns a persisted project' do
          _(project).must_be :persisted?
        end

        it 'sets the current user as the creator' do
          _(project.created_by).must_equal(Current.user)
        end

        it 'sets the current account' do
          _(project.account).must_equal(Current.account)
        end
      end
    end
  end
end
