class AddIdentifierToProjects < ActiveRecord::Migration[7.0]
  def up
    add_column :projects, :identifier, :string
    Project.all.each do |project|
      project.update(identifier: project.generate_identifier)
    end

    change_column_null :projects, :identifier, false
    add_index :projects, [:identifier, :account_id], unique: true
  end

  def down
    remove_column :projects, :identifier
  end
end
