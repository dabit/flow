# frozen_string_literal: true

module App
  module WorkItems
    class WorkItemStatusesController < App::BaseController
      before_action :find_project

      def update
        @work_item = @project.work_items.find(params[:work_item_id])
        Services::WorkItems::Transition.call(@work_item, params[:transition_to])

        redirect_to app_project_work_item_path(@project, @work_item)
      end
    end
  end
end
