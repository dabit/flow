class CreateWorkItemStatusEvents < ActiveRecord::Migration[7.0]
  def change
    create_table :work_item_status_events, id: :uuid do |t|
      t.belongs_to :work_item, null: false, foreign_key: true, type: :uuid
      t.belongs_to :work_item_status, null: false, foreign_key: true, type: :uuid
      t.belongs_to :user, null: false, foreign_key: true, type: :uuid
      t.integer :event_type, null: false
      t.integer :hours_spent, null: false, default: 0

      t.timestamps
    end
  end
end
