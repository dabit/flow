class CreateWorkItemStatuses < ActiveRecord::Migration[7.0]
  def change
    create_table :work_item_statuses, id: :uuid do |t|
      t.string :i18n_name
      t.integer :position

      t.timestamps
    end
  end
end
