# frozen_string_literal: true

# == Schema Information
#
# Table name: work_item_data
#
#  id                 :uuid             not null, primary key
#  cycle_time_hours   :integer
#  cycle_time_minutes :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  project_id         :uuid             not null
#  work_item_id       :uuid             not null
#
# Indexes
#
#  index_work_item_data_on_project_id    (project_id)
#  index_work_item_data_on_work_item_id  (work_item_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (work_item_id => work_items.id)
#
require 'test_helper'

class WorkItemDataTest < ActiveSupport::TestCase
  let(:project) { create(:project) }

  describe 'for_average scope' do
    before do
      create(:work_item_data, project:, updated_at: 1.week.ago)
      create(:work_item_data, project:, updated_at: (Rails.configuration.flowu.average_threshold_weeks.weeks + 1.day).ago)
    end

    it 'only returns those that are within the average treshold' do
      assert_equal 1, WorkItemData.for_average.count
    end
  end
end
