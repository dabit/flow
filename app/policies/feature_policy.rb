class FeaturePolicy < ApplicationPolicy
  def default_policy
    UNLEASH.enabled?('features')
  end
end
