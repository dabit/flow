# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require 'simplecov'

SimpleCov.start :rails do
  enable_coverage :branch
  add_filter 'app/channels/application_cable'
  add_filter 'app/jobs'
  add_filter 'app/controllers/devise/base_controller'
  add_filter 'vendor'
  add_filter 'app/dashboards'
  add_filter 'app/fields'
  add_filter 'app/controllers/admin'

  add_filter 'app/mailers'
  add_filter 'app/policies'

  minimum_coverage 90 if ENV['CI'] == 'true'
end

require_relative '../config/environment'
require 'rails/test_help'
require 'minitest/reporters'

Minitest::Reporters.use! [Minitest::Reporters::MeanTimeReporter.new(show_progress: false), Minitest::Reporters::SpecReporter.new]

module ActiveSupport
  class TestCase
    include ActionView::Helpers::TranslationHelper
    include FactoryBot::Syntax::Methods
    include Devise::Test::IntegrationHelpers

    # Run tests in parallel with specified workers
    parallelize(workers: :number_of_processors)

    # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
    # fixtures :all

    # Add more helper methods to be used by all tests here...
    parallelize_setup do |worker|
      SimpleCov.command_name "#{SimpleCov.command_name}-#{worker}"
    end

    parallelize_teardown do |_worker|
      SimpleCov.result
    end

    def all_item_statuses
      WorkItemStatus.valid_work_item_statuses.each do |status|
        create(:work_item_status, status)
      end
    end
  end
end
