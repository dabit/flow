# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    module Transitions
      class ToBacklogTest < ActiveSupport::TestCase
        let(:work_item) { create(:work_item, :in_progress) }
        let(:user) { work_item.assigned_to }

        before do
          all_item_statuses
          Current.user = user
        end

        describe 'backlog' do
          before do
            Services::WorkItems::Transition.call(work_item, 'backlog')
          end

          it 'should transition the work item backlog' do
            _(work_item.work_item_status).must_equal(create(:work_item_status, :backlog))
          end
        end
      end
    end
  end
end
