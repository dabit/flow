# frozen_string_literal: true

module App
  class ProfilesController < App::BaseController
    def show
      @profile = Current.user.profile
      redirect_to edit_app_profile_path unless @profile
    end

    def edit
      @profile = Current.user.profile || Current.user.build_profile
    end

    def create
      @profile = Current.user.build_profile(profile_params)
      @profile.save

      respond_with :app, @profile, location: -> { app_profile_path }
    end

    def update
      @profile = Current.user.profile
      @profile.update(profile_params)

      respond_with :app, @profile, location: -> { app_profile_path }
    end

    def page_title
      I18n.t(action_name, scope: [:page_title, controller_name])
    end

    private

    def profile_params
      params.require(:profile).permit(:first_name, :last_name, :time_zone)
    end
  end
end
