# frozen_string_literal: true

# == Schema Information
#
# Table name: work_item_status_events
#
#  id                  :uuid             not null, primary key
#  event_type          :integer          not null
#  hours_spent         :integer          default(0), not null
#  minutes_spent       :integer          default(0), not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  user_id             :uuid             not null
#  work_item_id        :uuid             not null
#  work_item_status_id :uuid             not null
#
# Indexes
#
#  index_work_item_status_events_on_user_id              (user_id)
#  index_work_item_status_events_on_work_item_id         (work_item_id)
#  index_work_item_status_events_on_work_item_status_id  (work_item_status_id)
#
# Foreign Keys
#
#  fk_rails_...  (user_id => users.id)
#  fk_rails_...  (work_item_id => work_items.id)
#  fk_rails_...  (work_item_status_id => work_item_statuses.id)
#
class WorkItemStatusEvent < ApplicationRecord
  belongs_to :work_item
  belongs_to :work_item_status
  belongs_to :user

  enum :event_type, { start: 0, end: 1 }

  default_scope -> { order(created_at: :asc) }
end
