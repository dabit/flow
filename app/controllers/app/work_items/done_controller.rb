# frozen_string_literal: true

module App
  module WorkItems
    class DoneController < App::WorkItemsController
      def index
        @work_items = @project.work_items.includes(:work_item_status, assigned_to: :profile).done.page(params[:page])
      end
    end
  end
end
