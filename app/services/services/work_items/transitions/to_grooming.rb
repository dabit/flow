# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToGrooming < Services::WorkItems::Transitions::BaseTransition
      end
    end
  end
end
