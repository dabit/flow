# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    module Transitions
      class ToReviewTest < ActiveSupport::TestCase
        let(:work_item) { create(:work_item, :backlog) }
        let(:user) { work_item.assigned_to }

        before do
          all_item_statuses
          Current.user = user
        end

        describe 'review' do
          before do
            Services::WorkItems::Transition.call(work_item, 'review')
          end

          it 'should transition the work item to review' do
            _(work_item.work_item_status).must_equal(create(:work_item_status, :review))
          end
        end
      end
    end
  end
end
