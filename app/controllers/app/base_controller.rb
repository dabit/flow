# frozen_string_literal: true

module App
  class BaseController < ApplicationController
    before_action :authenticate_user!
    before_action :set_current_user
    before_action :set_time_zone
    before_action :set_current_account

    layout 'app'

    def current_navbar_item; end
    helper_method :current_navbar_item

    protected

    def set_current_user
      Current.user = current_user
    end

    def set_current_account
      return if cookies.signed[:account_id].blank?

      begin
        Current.account = current_user.accounts.find(cookies.signed[:account_id])
      rescue ActiveRecord::RecordNotFound
        cookies.signed[:account_id] = nil
      end
    end

    def find_project
      @project = current_user.projects.find(params[:project_id])
    end

    def find_work_item
      @work_item = Current.user.work_items.find(params[:work_item_id])
    end

    def page_title
      @page_title ||= ''
    end
    helper_method :page_title

    def check_current_account
      redirect_to app_root_path if Current.account.nil?
    end

    def set_time_zone
      Time.zone = Current.time_zone
    end
  end
end
