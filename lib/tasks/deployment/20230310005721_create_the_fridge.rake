namespace :after_party do
  desc 'Deployment task: create_the_fridge'
  task create_the_fridge: :environment do
    puts "Running deploy task 'create_the_fridge'"

    WorkItemStatus.create(i18n_name: 'fridge')

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord
      .create version: AfterParty::TaskRecorder.new(__FILE__).timestamp
  end
end
