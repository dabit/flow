import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="navbar"
export default class extends Controller {
  static targets=['toggableElement', 'toggableMobileElement']
  toggle() {
    this.toggableElementTarget.classList.toggle('hidden')
  }

  toggleMobile() {
    this.toggableMobileElementTarget.classList.toggle('hidden')
  }
}
