# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: "noreply@#{Rails.configuration.flowu.mailer_domain}"
  layout 'mailer'
end
