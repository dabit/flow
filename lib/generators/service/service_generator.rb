class ServiceGenerator < Rails::Generators::NamedBase
  source_root File.expand_path("templates", __dir__)

  def create_service_file
    template 'service.rb.erb', "app/services/services/#{file_path}.rb"
  end

  def create_test_file
    template 'service_test.rb.erb', "test/services/#{file_path}_test.rb"
  end
end
