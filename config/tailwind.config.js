const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './public/*.html',
    './app/helpers/**/*.rb',
    './app/javascript/**/*.js',
    './app/views/**/*.{erb,haml,html,slim}'
  ],
  theme: {
    extend: {
      colors: {
        'primary': {
          DEFAULT: '#6096B4',
          50: '#FFFFFF',
          100: '#FFFFFF',
          200: '#EBF2F5',
          300: '#CFDFE8',
          400: '#B3CDDB',
          500: '#97BBCE',
          600: '#7CA8C1',
          700: '#6096B4',
          800: '#467995',
          900: '#355A6F'
        },
        'two': {
          DEFAULT: '#93BFCF',
          50: '#FFFFFF',
          100: '#FFFFFF',
          200: '#FFFFFF',
          300: '#FFFFFF',
          400: '#E8F1F5',
          500: '#CBE0E8',
          600: '#AFD0DC',
          700: '#93BFCF',
          800: '#6CA8BE',
          900: '#4A8FA7'
        },
        'white-rock': {
          DEFAULT: '#EEE9DA',
          50: '#FFFFFF',
          100: '#FFFFFF',
          200: '#FFFFFF',
          300: '#FFFFFF',
          400: '#FBFAF6',
          500: '#EEE9DA',
          600: '#DCD2B4',
          700: '#CBBB8D',
          800: '#B9A467',
          900: '#9F8949'
        },
      },
      fontFamily: {
        sans: ['Golos Text', ...defaultTheme.fontFamily.sans],
      },
      safelist: [
      ],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/aspect-ratio'),
    require('@tailwindcss/typography'),
  ]
}
