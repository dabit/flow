# frozen_string_literal: true

# == Schema Information
#
# Table name: work_items
#
#  id                   :uuid             not null, primary key
#  deleted_at           :datetime
#  identifier           :string           not null
#  intid                :bigint           not null
#  position             :integer          default(1), not null
#  stashed_in_fridge_at :datetime
#  title                :string
#  work_item_type       :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  assigned_to_id       :uuid
#  created_by_id        :uuid             not null
#  feature_id           :uuid
#  project_id           :uuid             not null
#  work_item_status_id  :uuid             not null
#
# Indexes
#
#  index_work_items_on_assigned_to_id       (assigned_to_id)
#  index_work_items_on_created_by_id        (created_by_id)
#  index_work_items_on_deleted_at           (deleted_at)
#  index_work_items_on_feature_id           (feature_id)
#  index_work_items_on_position             (position)
#  index_work_items_on_project_id           (project_id)
#  index_work_items_on_work_item_status_id  (work_item_status_id)
#
# Foreign Keys
#
#  fk_rails_...  (assigned_to_id => users.id)
#  fk_rails_...  (created_by_id => users.id)
#  fk_rails_...  (feature_id => features.id)
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (work_item_status_id => work_item_statuses.id)
#
require 'test_helper'

class WorkItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
