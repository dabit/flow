class AddBacklogToWorkItemStatuses < ActiveRecord::Migration[7.0]
  def change
    add_column :work_item_statuses, :backlog, :boolean, default: false
  end
end
