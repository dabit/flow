# frozen_string_literal: true

module App
  class ProjectsController < App::BaseController
    def show
      @project = Current.user.projects.find(params[:id])
      @work_items = @project.work_items.includes(:work_item_status, :assigned_to).limit(5).order(updated_at: :desc)
      @features = @project.features.limit(5).order(updated_at: :desc)
    end

    def new
      @project = Project.new(name: params[:name])
      @project.generate_identifier if @project.name.present?

      respond_to do |format|
        format.html { respond_with @project }
        format.turbo_stream
      end
    end

    def edit
      @project = Current.user.projects.find(params[:id])

      respond_with :app, @project
    end

    def create
      @project = Services::Projects::Create.call(project_params)

      respond_with :app, @project
    end

    def update
      @project = Current.user.projects.find(params[:id])
      @project.update(project_params)

      respond_with :app, @project
    end

    def current_navbar_item
      :projects
    end

    def page_title
      I18n.t(action_name, scope: [:page_title, controller_name], account_name: Current.account.name,
                          resource_name: @project&.name)
    end

    protected

    def project_params
      params.require(:project).permit(:name, :description, :identifier)
    end
  end
end
