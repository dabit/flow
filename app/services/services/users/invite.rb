# frozen_string_literal: true

module Services
  module Users
    class Invite < ApplicationService
      def call(email, account)
        User.invite!({ email: }, Current.user).tap do |user|
          return user if account.users.include?(user)

          AccountUser.create(user:, account:, role: :member)
        end
      end
    end
  end
end
