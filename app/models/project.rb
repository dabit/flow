# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id            :uuid             not null, primary key
#  deleted_at    :datetime
#  identifier    :string           not null
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  account_id    :uuid
#  created_by_id :uuid
#
# Indexes
#
#  index_projects_on_account_id                 (account_id)
#  index_projects_on_created_by_id              (created_by_id)
#  index_projects_on_deleted_at                 (deleted_at)
#  index_projects_on_identifier_and_account_id  (identifier,account_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (created_by_id => users.id)
#
class Project < ApplicationRecord
  IDENTIFIER_LENGTH = 5

  belongs_to :account
  belongs_to :created_by, class_name: 'User'
  has_many :work_items, dependent: :destroy
  has_many :features, dependent: :destroy
  has_many :work_item_data, dependent: :destroy, class_name: 'WorkItemData'

  validates :name, presence: true, length: { maximum: 100 }
  validates :identifier,
            presence: true,
            length: { maximum: IDENTIFIER_LENGTH },
            uniqueness: { scope: :account_id }

  has_rich_text :description

  # We do this in case we want to scope users to projects
  delegate :users, to: :account

  def to_s
    name
  end

  def average_cycle_time_hours
    work_item_data.for_average.average(:cycle_time_hours).to_i
  end

  def average_cycle_time_minutes
    work_item_data.for_average.average(:cycle_time_minutes).to_i
  end

  def generate_identifier
    self.identifier = name.gsub(/\W/, '').upcase.first(IDENTIFIER_LENGTH)
  end

  def self.generate_identifier(text)
    text.gsub(/\W/, '').upcase.first(IDENTIFIER_LENGTH)
  end
end
