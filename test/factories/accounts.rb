# frozen_string_literal: true

# == Schema Information
#
# Table name: accounts
#
#  id         :uuid             not null, primary key
#  deleted_at :datetime
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :account do
    name { FFaker::Company.name }

    transient do
      owner { create(:user) }
      project_count { 5 }
    end

    after(:create) do |account, evaluator|
      account.account_users.create(user: evaluator.owner, role: :owner) if evaluator.owner

      create_list(:project, evaluator.project_count, account:, created_by: evaluator.owner) if evaluator.project_count.positive?
    end
  end
end
