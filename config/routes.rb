# frozen_string_literal: true

# == Route Map
#
#                                   Prefix Verb   URI Pattern                                                                                       Controller#Action
#                         new_user_session GET    /users/sign_in(.:format)                                                                          devise/sessions#new
#                             user_session POST   /users/sign_in(.:format)                                                                          devise/sessions#create
#                     destroy_user_session DELETE /users/sign_out(.:format)                                                                         devise/sessions#destroy
#                        new_user_password GET    /users/password/new(.:format)                                                                     devise/passwords#new
#                       edit_user_password GET    /users/password/edit(.:format)                                                                    devise/passwords#edit
#                            user_password PATCH  /users/password(.:format)                                                                         devise/passwords#update
#                                          PUT    /users/password(.:format)                                                                         devise/passwords#update
#                                          POST   /users/password(.:format)                                                                         devise/passwords#create
#                 cancel_user_registration GET    /users/cancel(.:format)                                                                           devise/toggable_registrations#cancel
#                    new_user_registration GET    /users/sign_up(.:format)                                                                          devise/toggable_registrations#new
#                   edit_user_registration GET    /users/edit(.:format)                                                                             devise/toggable_registrations#edit
#                        user_registration PATCH  /users(.:format)                                                                                  devise/toggable_registrations#update
#                                          PUT    /users(.:format)                                                                                  devise/toggable_registrations#update
#                                          DELETE /users(.:format)                                                                                  devise/toggable_registrations#destroy
#                                          POST   /users(.:format)                                                                                  devise/toggable_registrations#create
#                   accept_user_invitation GET    /users/invitation/accept(.:format)                                                                devise/invitations#edit
#                   remove_user_invitation GET    /users/invitation/remove(.:format)                                                                devise/invitations#destroy
#                      new_user_invitation GET    /users/invitation/new(.:format)                                                                   devise/invitations#new
#                          user_invitation PATCH  /users/invitation(.:format)                                                                       devise/invitations#update
#                                          PUT    /users/invitation(.:format)                                                                       devise/invitations#update
#                                          POST   /users/invitation(.:format)                                                                       devise/invitations#create
#                                     root GET    /                                                                                                 home#show
#                                 app_root GET    /app(.:format)                                                                                    app/accounts#index
#                             app_accounts GET    /app/accounts(.:format)                                                                           app/accounts#index
#                                          POST   /app/accounts(.:format)                                                                           app/accounts#create
#                          new_app_account GET    /app/accounts/new(.:format)                                                                       app/accounts#new
#                         edit_app_account GET    /app/accounts/:id/edit(.:format)                                                                  app/accounts#edit
#                              app_account GET    /app/accounts/:id(.:format)                                                                       app/accounts#show
#                                          PATCH  /app/accounts/:id(.:format)                                                                       app/accounts#update
#                                          PUT    /app/accounts/:id(.:format)                                                                       app/accounts#update
#                                          DELETE /app/accounts/:id(.:format)                                                                       app/accounts#destroy
#                                app_users GET    /app/users(.:format)                                                                              app/users#index
#                                          POST   /app/users(.:format)                                                                              app/users#create
#                             new_app_user GET    /app/users/new(.:format)                                                                          app/users#new
#                            edit_app_user GET    /app/users/:id/edit(.:format)                                                                     app/users#edit
#                                 app_user GET    /app/users/:id(.:format)                                                                          app/users#show
#                                          PATCH  /app/users/:id(.:format)                                                                          app/users#update
#                                          PUT    /app/users/:id(.:format)                                                                          app/users#update
#                                          DELETE /app/users/:id(.:format)                                                                          app/users#destroy
#         app_project_work_items_withdrawn GET    /app/projects/:project_id/work_items/withdrawn(.:format)                                          app/work_items/withdrawn#index
#              app_project_work_items_done GET    /app/projects/:project_id/work_items/done(.:format)                                               app/work_items/done#index
#            app_project_work_items_fridge GET    /app/projects/:project_id/work_items/fridge(.:format)                                             app/work_items/fridge#index
#   app_project_work_item_work_item_status PATCH  /app/projects/:project_id/work_items/:work_item_id/work_item_status(.:format)                     app/work_items/work_item_statuses#update
#                                          PUT    /app/projects/:project_id/work_items/:work_item_id/work_item_status(.:format)                     app/work_items/work_item_statuses#update
#                   app_project_work_items GET    /app/projects/:project_id/work_items(.:format)                                                    app/work_items#index
#                                          POST   /app/projects/:project_id/work_items(.:format)                                                    app/work_items#create
#                new_app_project_work_item GET    /app/projects/:project_id/work_items/new(.:format)                                                app/work_items#new
#               edit_app_project_work_item GET    /app/projects/:project_id/work_items/:id/edit(.:format)                                           app/work_items#edit
#                    app_project_work_item GET    /app/projects/:project_id/work_items/:id(.:format)                                                app/work_items#show
#                                          PATCH  /app/projects/:project_id/work_items/:id(.:format)                                                app/work_items#update
#                                          PUT    /app/projects/:project_id/work_items/:id(.:format)                                                app/work_items#update
#                                          DELETE /app/projects/:project_id/work_items/:id(.:format)                                                app/work_items#destroy
#             app_project_work_item_groups GET    /app/projects/:project_id/work_item_groups(.:format)                                              app/work_item_groups#index
#                     app_project_features GET    /app/projects/:project_id/features(.:format)                                                      app/features#index
#                                          POST   /app/projects/:project_id/features(.:format)                                                      app/features#create
#                  new_app_project_feature GET    /app/projects/:project_id/features/new(.:format)                                                  app/features#new
#                 edit_app_project_feature GET    /app/projects/:project_id/features/:id/edit(.:format)                                             app/features#edit
#                      app_project_feature GET    /app/projects/:project_id/features/:id(.:format)                                                  app/features#show
#                                          PATCH  /app/projects/:project_id/features/:id(.:format)                                                  app/features#update
#                                          PUT    /app/projects/:project_id/features/:id(.:format)                                                  app/features#update
#                                          DELETE /app/projects/:project_id/features/:id(.:format)                                                  app/features#destroy
#                             app_projects GET    /app/projects(.:format)                                                                           app/projects#index
#                                          POST   /app/projects(.:format)                                                                           app/projects#create
#                          new_app_project GET    /app/projects/new(.:format)                                                                       app/projects#new
#                         edit_app_project GET    /app/projects/:id/edit(.:format)                                                                  app/projects#edit
#                              app_project GET    /app/projects/:id(.:format)                                                                       app/projects#show
#                                          PATCH  /app/projects/:id(.:format)                                                                       app/projects#update
#                                          PUT    /app/projects/:id(.:format)                                                                       app/projects#update
#                                          DELETE /app/projects/:id(.:format)                                                                       app/projects#destroy
#                   app_work_item_comments GET    /app/work_item/:work_item_id/comments(.:format)                                                   app/work_items/comments#index
#                                          POST   /app/work_item/:work_item_id/comments(.:format)                                                   app/work_items/comments#create
#                new_app_work_item_comment GET    /app/work_item/:work_item_id/comments/new(.:format)                                               app/work_items/comments#new
#               edit_app_work_item_comment GET    /app/work_item/:work_item_id/comments/:id/edit(.:format)                                          app/work_items/comments#edit
#                    app_work_item_comment GET    /app/work_item/:work_item_id/comments/:id(.:format)                                               app/work_items/comments#show
#                                          PATCH  /app/work_item/:work_item_id/comments/:id(.:format)                                               app/work_items/comments#update
#                                          PUT    /app/work_item/:work_item_id/comments/:id(.:format)                                               app/work_items/comments#update
#                                          DELETE /app/work_item/:work_item_id/comments/:id(.:format)                                               app/work_items/comments#destroy
#                   app_work_item_position PATCH  /app/work_item/:work_item_id/position(.:format)                                                   app/work_items/positions#update
#                                          PUT    /app/work_item/:work_item_id/position(.:format)                                                   app/work_items/positions#update
#                         edit_app_profile GET    /app/profile/edit(.:format)                                                                       app/profiles#edit
#                              app_profile GET    /app/profile(.:format)                                                                            app/profiles#show
#                                          PATCH  /app/profile(.:format)                                                                            app/profiles#update
#                                          PUT    /app/profile(.:format)                                                                            app/profiles#update
#                                          POST   /app/profile(.:format)                                                                            app/profiles#create
#         turbo_recede_historical_location GET    /recede_historical_location(.:format)                                                             turbo/native/navigation#recede
#         turbo_resume_historical_location GET    /resume_historical_location(.:format)                                                             turbo/native/navigation#resume
#        turbo_refresh_historical_location GET    /refresh_historical_location(.:format)                                                            turbo/native/navigation#refresh
#            rails_postmark_inbound_emails POST   /rails/action_mailbox/postmark/inbound_emails(.:format)                                           action_mailbox/ingresses/postmark/inbound_emails#create
#               rails_relay_inbound_emails POST   /rails/action_mailbox/relay/inbound_emails(.:format)                                              action_mailbox/ingresses/relay/inbound_emails#create
#            rails_sendgrid_inbound_emails POST   /rails/action_mailbox/sendgrid/inbound_emails(.:format)                                           action_mailbox/ingresses/sendgrid/inbound_emails#create
#      rails_mandrill_inbound_health_check GET    /rails/action_mailbox/mandrill/inbound_emails(.:format)                                           action_mailbox/ingresses/mandrill/inbound_emails#health_check
#            rails_mandrill_inbound_emails POST   /rails/action_mailbox/mandrill/inbound_emails(.:format)                                           action_mailbox/ingresses/mandrill/inbound_emails#create
#             rails_mailgun_inbound_emails POST   /rails/action_mailbox/mailgun/inbound_emails/mime(.:format)                                       action_mailbox/ingresses/mailgun/inbound_emails#create
#           rails_conductor_inbound_emails GET    /rails/conductor/action_mailbox/inbound_emails(.:format)                                          rails/conductor/action_mailbox/inbound_emails#index
#                                          POST   /rails/conductor/action_mailbox/inbound_emails(.:format)                                          rails/conductor/action_mailbox/inbound_emails#create
#        new_rails_conductor_inbound_email GET    /rails/conductor/action_mailbox/inbound_emails/new(.:format)                                      rails/conductor/action_mailbox/inbound_emails#new
#       edit_rails_conductor_inbound_email GET    /rails/conductor/action_mailbox/inbound_emails/:id/edit(.:format)                                 rails/conductor/action_mailbox/inbound_emails#edit
#            rails_conductor_inbound_email GET    /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                                      rails/conductor/action_mailbox/inbound_emails#show
#                                          PATCH  /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                                      rails/conductor/action_mailbox/inbound_emails#update
#                                          PUT    /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                                      rails/conductor/action_mailbox/inbound_emails#update
#                                          DELETE /rails/conductor/action_mailbox/inbound_emails/:id(.:format)                                      rails/conductor/action_mailbox/inbound_emails#destroy
# new_rails_conductor_inbound_email_source GET    /rails/conductor/action_mailbox/inbound_emails/sources/new(.:format)                              rails/conductor/action_mailbox/inbound_emails/sources#new
#    rails_conductor_inbound_email_sources POST   /rails/conductor/action_mailbox/inbound_emails/sources(.:format)                                  rails/conductor/action_mailbox/inbound_emails/sources#create
#    rails_conductor_inbound_email_reroute POST   /rails/conductor/action_mailbox/:inbound_email_id/reroute(.:format)                               rails/conductor/action_mailbox/reroutes#create
# rails_conductor_inbound_email_incinerate POST   /rails/conductor/action_mailbox/:inbound_email_id/incinerate(.:format)                            rails/conductor/action_mailbox/incinerates#create
#                       rails_service_blob GET    /rails/active_storage/blobs/redirect/:signed_id/*filename(.:format)                               active_storage/blobs/redirect#show
#                 rails_service_blob_proxy GET    /rails/active_storage/blobs/proxy/:signed_id/*filename(.:format)                                  active_storage/blobs/proxy#show
#                                          GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                                        active_storage/blobs/redirect#show
#                rails_blob_representation GET    /rails/active_storage/representations/redirect/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations/redirect#show
#          rails_blob_representation_proxy GET    /rails/active_storage/representations/proxy/:signed_blob_id/:variation_key/*filename(.:format)    active_storage/representations/proxy#show
#                                          GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format)          active_storage/representations/redirect#show
#                       rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                                       active_storage/disk#show
#                update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                               active_storage/disk#update
#                     rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                                    active_storage/direct_uploads#create

Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'devise/toggable_registrations', invitations: 'devise/invitations' }
  root 'home#show'

  namespace :app do
    root 'accounts#index'

    resources :accounts
    resources :users
    resources :projects do
      get 'work_items/withdrawn', to: 'work_items/withdrawn#index'
      get 'work_items/done', to: 'work_items/done#index'
      get 'work_items/fridge', to: 'work_items/fridge#index'
      resources :work_items do
        resource :work_item_status, only: :update, controller: 'work_items/work_item_statuses'
      end
      resources :work_item_groups, only: [:index]
      resources :features
    end

    scope 'work_item/:work_item_id', module: 'work_items', as: :work_item do
      resources :comments
      resource :position, only: [:update]
    end

    resource :profile, only: %i[create edit update show]
  end
end
