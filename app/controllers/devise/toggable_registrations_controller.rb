# frozen_string_literal: true

module Devise
  class ToggableRegistrationsController < Devise::RegistrationsController
    def new
      if Rails.configuration.flowu.enable_registration
        super
      else
        redirect_to root_path
      end
    end

    def create
      if UNLEASH.enabled?('public_sign_up', @unleash_context)
        super
      else
        redirect_to root_path
      end
    end
  end
end
