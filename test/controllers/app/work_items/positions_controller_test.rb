# frozen_string_literal: true

require 'test_helper'

module App
  module WorkItems
    class PositionsControllerTest < ActionDispatch::IntegrationTest
      let(:user) { create(:user) }
      let(:work_item) { create(:work_item) }

      before do
        sign_in(user)
      end

      describe 'PATCH update' do
        it 'returns http success' do
          patch app_work_item_position_path(work_item), params: { position: 1 }

          assert_response :success
        end
      end
    end
  end
end
