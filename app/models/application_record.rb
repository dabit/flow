# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  def self.inherited(subclass)
    super
    return unless subclass.has_attribute?(:deleted_at)

    setup_for_soft_delete(subclass)
  rescue ActiveRecord::NoDatabaseError, ActiveRecord::StatementInvalid
    nil
  end

  def self.setup_for_soft_delete(subclass)
    subclass.send(:default_scope, -> { where(deleted_at: nil) })

    class << subclass
      def archived
        where.not(deleted_at: nil)
      end
    end

    subclass.define_method(:destroy) do
      touch(:deleted_at)
    end
  end

  def self.human_enum_name(enum_name, enum_value)
    I18n.t("activerecord.attributes.#{model_name.i18n_key}.#{enum_name.to_s.pluralize}.#{enum_value}")
  end

  def self.plural_name
    model_name.human(count: 2)
  end

  def self.enum_collection_for_select(enum_plural_name)
    send(enum_plural_name).collect { |k, _v| [k, human_enum_name(enum_plural_name, k)] }
  end
end
