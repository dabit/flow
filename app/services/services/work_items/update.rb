# frozen_string_literal: true

module Services
  module WorkItems
    class Update < Services::ApplicationService
      def call(work_item, work_item_params)
        WorkItem.transaction do
          work_item.assign_attributes(work_item_params)
          if work_item.work_item_status_changed?
            close_current_event(work_item)
            create_event_record(work_item)
          end
          work_item.save
        end
      end

      def close_current_event(work_item)
        last_event = work_item.work_item_status_events.last
        return unless last_event

        hours_spent = ((Time.current - last_event.created_at) / 1.hour).ceil
        minutes_spent = ((Time.current - last_event.created_at) / 1.minute).ceil

        work_item.work_item_status_events.create(
          user: Current.user,
          work_item_status: last_event.work_item_status,
          event_type: :end,
          hours_spent:,
          minutes_spent:
        )
      end

      def create_event_record(work_item)
        work_item.work_item_status_events.create(
          user: Current.user,
          work_item_status: work_item.work_item_status,
          event_type: :start
        )
      end
    end
  end
end
