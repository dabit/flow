import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="notification"
export default class extends Controller {
  connect() {
    setTimeout(() => { this.show() }, 100)
    setTimeout(() => { this.hide() }, 5000)

    this.element.classList.add('transform', 'ease-out', 'duration-500', 'transition')
    this.element.classList.add('translate-y-2', 'opacity-0', 'sm:translate-y-0', 'sm:translate-x-2')
  }

  show() {
    this.element.classList.add('translate-y-0', 'opacity-100', 'sm:translate-x-0')
  }

  hide() {
    this.element.classList.remove('opacity-100')
    this.element.classList.add('ease-in', 'opacity-0')
  }

  closeDialog() {
    this.element.classList.add('hidden')
  }
}
