class CreateProjects < ActiveRecord::Migration[7.0]
  def change
    create_table :projects, id: :uuid do |t|
      t.string :name
      t.belongs_to :created_by, foreign_key: { to_table: :users }, type: :uuid
      t.belongs_to :account, type: :uuid
      t.timestamp :deleted_at, index: true

      t.timestamps
    end
  end
end
