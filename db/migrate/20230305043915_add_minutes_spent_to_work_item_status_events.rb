class AddMinutesSpentToWorkItemStatusEvents < ActiveRecord::Migration[7.0]
  def change
    add_column :work_item_status_events, :minutes_spent, :integer, null: false, default: 0
  end
end
