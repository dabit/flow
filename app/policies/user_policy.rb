class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end

  def create?
    Current.role.owner?
  end

  def index?
    true
  end
end
