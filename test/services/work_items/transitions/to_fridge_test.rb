# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    module Transitions
      class ToFridgeTest < ActiveSupport::TestCase
        let(:work_item) { create(:work_item, :backlog) }
        let(:user) { work_item.assigned_to }

        before do
          all_item_statuses
          Current.user = user
        end

        describe 'fridge' do
          before do
            Services::WorkItems::Transition.call(work_item, 'fridge')
          end

          it 'should transition the work item to fridge' do
            _(work_item.work_item_status).must_equal(create(:work_item_status, :fridge))
          end

          it 'should write the stashed_in_fridge_at to the work_item' do
            _(work_item.stashed_in_fridge_at).wont_be_nil
          end
        end
      end
    end
  end
end
