# frozen_string_literal: true

require 'test_helper'

class UserSignupTest < ActionDispatch::IntegrationTest
  describe 'User signup' do
    let(:first_name) { FFaker::Name.first_name }
    let(:last_name) { FFaker::Name.last_name }

    it 'signs up' do
      get new_user_registration_path
      assert_response :success

      post user_registration_path, params: {
        user: {
          email: FFaker::Internet.email,
          password: User::TEST_PASSWORD,
          password_confirmation: User::TEST_PASSWORD,
          profile_attributes: {
            first_name:,
            last_name:,
            time_zone: 'Eastern Time (US & Canada)'
          }
        }
      }

      assert_redirected_to app_root_path
      follow_redirect!

      get app_profile_path
      assert_response :success
      assert_select 'div', first_name
      assert_select 'div', last_name
    end
  end
end
