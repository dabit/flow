# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    module Transitions
      class ToDoneTest < ActiveSupport::TestCase
        let(:work_item) { create(:work_item, :done) }
        let(:user) { work_item.assigned_to }

        before do
          all_item_statuses
          Current.user = user
        end

        describe 'done' do
          before do
            Services::WorkItems::Transition.call(work_item, 'done')
          end

          it 'should transition the work item to done' do
            _(work_item.work_item_status).must_equal(create(:work_item_status, :done))
          end

          it 'should write the work_item_data to the work_item' do
            _(work_item.work_item_data).wont_be_nil
          end
        end
      end
    end
  end
end
