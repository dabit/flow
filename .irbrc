# frozen_string_literal: true

require 'irb/completion'

IRB.conf[:USE_AUTOCOMPLETE] = false
