# frozen_string_literal: true

require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  it 'should be successful' do
    get root_path
    assert_response :success
  end
end
