# frozen_string_literal: true

# == Schema Information
#
# Table name: work_items
#
#  id                   :uuid             not null, primary key
#  deleted_at           :datetime
#  identifier           :string           not null
#  intid                :bigint           not null
#  position             :integer          default(1), not null
#  stashed_in_fridge_at :datetime
#  title                :string
#  work_item_type       :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#  assigned_to_id       :uuid
#  created_by_id        :uuid             not null
#  feature_id           :uuid
#  project_id           :uuid             not null
#  work_item_status_id  :uuid             not null
#
# Indexes
#
#  index_work_items_on_assigned_to_id       (assigned_to_id)
#  index_work_items_on_created_by_id        (created_by_id)
#  index_work_items_on_deleted_at           (deleted_at)
#  index_work_items_on_feature_id           (feature_id)
#  index_work_items_on_position             (position)
#  index_work_items_on_project_id           (project_id)
#  index_work_items_on_work_item_status_id  (work_item_status_id)
#
# Foreign Keys
#
#  fk_rails_...  (assigned_to_id => users.id)
#  fk_rails_...  (created_by_id => users.id)
#  fk_rails_...  (feature_id => features.id)
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (work_item_status_id => work_item_statuses.id)
#
class WorkItem < ApplicationRecord
  acts_as_list scope: %i[project_id work_item_status_id]

  belongs_to :project
  belongs_to :created_by, class_name: 'User'
  belongs_to :assigned_to, class_name: 'User', optional: true
  belongs_to :work_item_status
  belongs_to :feature, optional: true

  has_many :work_item_status_events, dependent: :destroy
  has_many :comments, as: :commentable, dependent: :destroy
  has_one :work_item_data, dependent: :destroy

  enum work_item_type: { story: 0, task: 1, bug: 2 }

  has_rich_text :description

  validates :title, presence: true, length: { maximum: 255, minimum: 10 }

  WorkItemStatus.valid_work_item_statuses.each do |status|
    scope status, -> { joins(:work_item_status).where(work_item_statuses: { i18n_name: status }) }
    delegate "#{status}?", to: :work_item_status
  end

  def to_s
    "[#{identifier}] #{title}"
  end
end
