# frozen_string_literal: true

module Services
  module WorkItemData
    class Generate < ApplicationService
      attr_accessor :work_item

      def call(work_item)
        self.work_item = work_item
        return unless work_item.done?

        work_item_data = work_item.work_item_data || ::WorkItemData.new(work_item:, project: work_item.project)
        work_item_data_hours(work_item_data)
        work_item_data_minutes(work_item_data)
        work_item_data.save
      end

      private

      def work_item_data_hours(work_item_data)
        work_item_data.cycle_time_hours = cycle_time_difference / 1.hour
      end

      def work_item_data_minutes(work_item_data)
        work_item_data.cycle_time_minutes = cycle_time_difference / 1.minute
      end

      def event_list
        work_item.work_item_status_events
      end

      def cycle_time_difference
        event_list.last.created_at - event_list.first.created_at
      end
    end
  end
end
