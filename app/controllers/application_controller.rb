# frozen_string_literal: true

require 'application_responder'

class ApplicationController < ActionController::Base
  include Pundit::Authorization

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :set_unleash_context

  self.responder = ApplicationResponder
  respond_to :html

  def after_sign_in_path_for(_resource)
    app_root_path
  end

  def after_accept_path_for(_resource)
    app_root_path
  end

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up,
                                      keys: [:email, :password, :password_confirmation,
                                             { profile_attributes: %i[first_name last_name time_zone] }])
    devise_parameter_sanitizer.permit(:accept_invitation,
                                      keys: [:email, :password, :password_confirmation,
                                             { profile_attributes: %i[first_name last_name time_zone] }])
  end

  protected

  def set_unleash_context
    @unleash_context = Unleash::Context.new(
      session_id: session.id,
      remote_address: request.remote_ip,
      user_id: Current.user&.id
    )
  end
end
