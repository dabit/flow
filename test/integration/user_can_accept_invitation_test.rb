# frozen_string_literal: true

require 'test_helper'

class UserCanAcceptInvitationTest < ActionDispatch::IntegrationTest
  let(:first_name) { FFaker::Name.first_name }
  let(:last_name) { FFaker::Name.last_name }
  let(:email) { FFaker::Internet.email }
  let(:account) { create(:account) }

  describe 'User can accept invitation' do
    it 'creates the user with profile and redirects to app root' do
      user = User.invite!(email:)
      account.account_users.create(user:, role: :member)

      get accept_user_invitation_path(@user, invitation_token: user.raw_invitation_token)
      assert_response :success

      put user_invitation_path, params: {
        user: {
          invitation_token: user.raw_invitation_token,
          password: User::TEST_PASSWORD,
          password_confirmation: User::TEST_PASSWORD,
          profile_attributes: {
            first_name:,
            last_name:,
            time_zone: 'Eastern Time (US & Canada)'
          }
        }
      }

      assert_redirected_to app_root_path
      follow_redirect!

      get app_profile_path
      assert_response :success
      assert_select 'div', first_name
      assert_select 'div', last_name
    end
  end
end
