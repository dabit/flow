# frozen_string_literal: true

require 'test_helper'

module App
  module WorkItems
    class DoneControllerTest < ActionDispatch::IntegrationTest
      let(:project) { create(:project) }
      let(:account) { project.account }
      let(:user) { account.owner }

      before do
        sign_in user
        create(:work_item, :done, project:)
        get app_account_path(account)
      end

      describe 'GET #index' do
        it 'returns http success' do
          get app_project_work_items_done_path(project)

          assert_response :success
        end
      end
    end
  end
end
