class CreateComments < ActiveRecord::Migration[7.0]
  def change
    create_table :comments, id: :uuid do |t|
      t.belongs_to :user, null: false, foreign_key: true, type: :uuid
      t.belongs_to :commentable, null: false, type: :uuid, polymorphic: true
      t.text :message

      t.timestamp :deleted_at

      t.timestamps
    end
  end
end
