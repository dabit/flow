class AddStashedInFridgeAtToWorkItems < ActiveRecord::Migration[7.0]
  def change
    add_column :work_items, :stashed_in_fridge_at, :timestamp
  end
end
