# frozen_string_literal: true

module App
  class WorkItemsController < App::BaseController
    before_action :find_project

    def index
      @work_items = @project.work_items
                            .includes(:work_item_status, assigned_to: :profile)
                            .where(work_item_statuses: { i18n_name: %w[backlog review in_progress ready_for_dev grooming] })
                            .group_by { |wi| wi.work_item_status.i18n_name }
      @draggable = true
    end

    def show
      @work_item = @project.work_items.find(params[:id])
      @users = @project.users.includes(:profile).order(:email)
    end

    def new
      @work_item = @project.work_items.new
      @users = @project.users.includes(:profile).order(:email)
    end

    def edit
      @work_item = @project.work_items.find(params[:id])
      @users = @project.users.includes(:profile).order(:email)

      respond_with :app, @project, @work_item
    end

    def create
      @work_item = Services::WorkItems::Create.call(@project, work_item_params)
      @users = @project.users.includes(:profile).order(:email)

      respond_with :app, @project, @work_item
    end

    def update
      @work_item = @project.work_items.find(params[:id])
      @users = @project.users.includes(:profile).order(:email)
      Services::WorkItems::Update.call(@work_item, work_item_params)

      respond_to do |format|
        format.html { respond_with :app, @project, @work_item }
        format.turbo_stream
      end
    end

    def page_title
      I18n.t(action_name, scope: [:page_title, controller_name], account_name: Current.account.name, project_name: @project,
                          resource_name: @work_item)
    end

    def work_item_query
      @work_item_query ||= @project.work_items.includes(:work_item_status, assigned_to: :profile).order(:position)
    end

    def navigation_options
      {
        'app.work_items.tabs.active': app_project_work_items_path,
        'app.work_items.tabs.done': app_project_work_items_done_path,
        'app.work_items.tabs.fridge': app_project_work_items_fridge_path,
        'app.work_items.tabs.withdrawn': app_project_work_items_withdrawn_path
      }
    end
    helper_method :navigation_options

    protected

    def current_navbar_item
      :work_items
    end

    def work_item_params
      params.require(:work_item).permit(:title,
                                        :work_item_type,
                                        :assigned_to_id,
                                        :description,
                                        :work_item_status_id,
                                        :feature_id)
    end
  end
end
