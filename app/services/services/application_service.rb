# frozen_string_literal: true

module Services
  class ApplicationService
    def self.call(*args)
      new.call(*args)
    end
  end
end
