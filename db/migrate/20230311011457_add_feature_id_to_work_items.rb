class AddFeatureIdToWorkItems < ActiveRecord::Migration[7.0]
  def change
    add_reference :work_items, :feature, foreign_key: true, type: :uuid
  end
end
