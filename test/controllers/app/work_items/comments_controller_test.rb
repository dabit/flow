# frozen_string_literal: true

require 'test_helper'

module App
  module WorkItems
    class CommentsControllerTest < ActionDispatch::IntegrationTest
      let(:work_item) { create(:work_item) }
      let(:user) { create(:user, with_account: work_item.project.account) }

      before { sign_in user }

      describe 'GET #new' do
        it 'returns http success' do
          get new_app_work_item_comment_path(work_item)

          assert_response :success
        end
      end

      describe 'POST #create' do
        describe 'turbo_stream' do
          it 'returns http success' do
            post app_work_item_comments_path(work_item, format: :turbo_stream),
                 params: { comment: { message: FFaker::Lorem.paragraph } }

            assert_response :success
          end
        end
      end

      describe 'GET #index' do
        before do
          create_list(:comment, 20, commentable: work_item)
        end

        it 'returns http success' do
          get app_work_item_comments_path(work_item)

          assert_response :success
        end
      end

      describe 'DELTE #destroy' do
        let(:comment) { create(:comment, commentable: work_item, user:) }

        describe 'turbo_stream' do
          it 'returns http success' do
            delete app_work_item_comment_path(work_item, comment, format: :turbo_stream)

            assert_response :success
          end
        end
      end
    end
  end
end
