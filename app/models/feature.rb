# frozen_string_literal: true

# == Schema Information
#
# Table name: features
#
#  id         :uuid             not null, primary key
#  deleted_at :datetime
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  project_id :uuid             not null
#  user_id    :uuid             not null
#
# Indexes
#
#  index_features_on_deleted_at  (deleted_at)
#  index_features_on_project_id  (project_id)
#  index_features_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (user_id => users.id)
#
class Feature < ApplicationRecord
  belongs_to :user
  belongs_to :project

  has_many :work_items, dependent: :nullify

  has_rich_text :description

  def to_s
    name
  end

  def work_items_total
    work_items.count
  end

  def work_items_done
    work_items.done.count
  end

  def progress
    return 0.0 if work_items_total.zero?

    work_items_done.to_f / work_items_total * 100
  end
end
