class AddTimeZoneToProfile < ActiveRecord::Migration[7.0]
  def change
    add_column :profiles, :time_zone, :string
  end
end
