# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToReadyForDev < Services::WorkItems::Transitions::BaseTransition
      end
    end
  end
end
