# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    class CreateTest < ActiveSupport::TestCase
      let(:project) { create(:project) }
      let(:user) { project.created_by }

      describe 'call' do
        let(:work_item) do
          work_item = build(:work_item, :backlog)
          Services::WorkItems::Create.new.call(
            project,
            {
              title: work_item.title,
              work_item_type: work_item.work_item_type,
              assigned_to_id: work_item.assigned_to_id,
              description: work_item.description,
              work_item_status_id: work_item.work_item_status_id
            }
          )
        end

        before do
          Current.user = user
        end

        it 'returns a persisted work item' do
          _(work_item).must_be :persisted?
        end

        it 'creates a work item status event' do
          _(work_item.work_item_status_events).wont_be_empty
        end

        it 'assigns the identifier' do
          _(work_item.identifier).must_equal "#{project.identifier}-#{work_item.intid}"
        end

        describe 'more work items exist' do
          before do
            create(:work_item, :done, project:, intid: 99)
          end

          it 'assigns the next intid' do
            _(work_item.intid).must_equal 100
          end
        end
      end
    end
  end
end
