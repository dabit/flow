# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItemData
    class GenerateTest < ActiveSupport::TestCase
      let(:work_item) { create(:work_item, :done) }

      describe '.call' do
        before do
          create(:work_item_status_event, event_type: :start, work_item:, work_item_status: create(:work_item_status, :backlog),
                                          created_at: 11.hours.ago)
          create(:work_item_status_event, event_type: :end, work_item:, work_item_status: create(:work_item_status, :backlog),
                                          created_at: 1.hour.ago)
          create(:work_item_status_event, event_type: :start, work_item:, work_item_status: create(:work_item_status, :done),
                                          created_at: 1.hour.ago)

          Services::WorkItemData::Generate.call(work_item)
        end

        describe 'work_item_data' do
          it 'calculates 10 hours of ttm' do
            _(work_item.work_item_data.cycle_time_hours).must_equal(10)
          end

          it 'calculates 600 minutes of ttm' do
            _(work_item.work_item_data.cycle_time_minutes).must_equal(659)
          end
        end
      end
    end
  end
end
