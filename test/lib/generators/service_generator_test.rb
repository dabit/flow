# frozen_string_literal: true

require 'test_helper'
require 'generators/service/service_generator'

class ServiceGeneratorTest < Rails::Generators::TestCase
  tests ServiceGenerator
  destination Rails.root.join('tmp/generators')
  setup :prepare_destination

  test 'generator runs without errors' do
    assert_nothing_raised do
      run_generator ['projects/create']
    end
  end

  test 'should add the service file to app/services/services' do
    run_generator ['projects/create']

    assert_file 'app/services/services/projects/create.rb'
  end

  test 'should add the service test file to test/services' do
    run_generator ['projects/create']

    assert_file 'test/services/projects/create_test.rb'
  end
end
