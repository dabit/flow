# frozen_string_literal: true

# == Schema Information
#
# Table name: features
#
#  id         :uuid             not null, primary key
#  deleted_at :datetime
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  project_id :uuid             not null
#  user_id    :uuid             not null
#
# Indexes
#
#  index_features_on_deleted_at  (deleted_at)
#  index_features_on_project_id  (project_id)
#  index_features_on_user_id     (user_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (user_id => users.id)
#
FactoryBot.define do
  factory :feature do
    name { FFaker::Lorem.sentence }
    description { FFaker::Lorem.paragraph }
    user
    project
  end
end
