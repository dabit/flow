# frozen_string_literal: true

# == Schema Information
#
# Table name: work_item_data
#
#  id                 :uuid             not null, primary key
#  cycle_time_hours   :integer
#  cycle_time_minutes :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  project_id         :uuid             not null
#  work_item_id       :uuid             not null
#
# Indexes
#
#  index_work_item_data_on_project_id    (project_id)
#  index_work_item_data_on_work_item_id  (work_item_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (work_item_id => work_items.id)
#
FactoryBot.define do
  factory :work_item_data do
    work_item
    project
    cycle_time_hours { rand(336) }
    cycle_time_minutes { rand(20_160) }
  end
end
