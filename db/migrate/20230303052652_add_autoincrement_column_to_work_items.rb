class AddAutoincrementColumnToWorkItems < ActiveRecord::Migration[7.0]
  def change
    add_column :work_items, :intid, :bigserial, index: true, unique: true
  end
end
