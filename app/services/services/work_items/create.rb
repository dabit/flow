# frozen_string_literal: true

module Services
  module WorkItems
    class Create < Services::ApplicationService
      def call(project, work_item_params)
        project.work_items.build(work_item_params).tap do |work_item|
          work_item.intid = WorkItem.unscoped.where(project:).maximum(:intid).to_i + 1
          assign_identifier(work_item)
          work_item.created_by = Current.user
          create_event_record(work_item) if work_item.save
        end
      end

      def assign_identifier(work_item)
        work_item.identifier = "#{work_item.project.identifier}-#{work_item.intid}"
      end

      def create_event_record(work_item)
        work_item.work_item_status_events.create(
          user: Current.user,
          work_item_status: work_item.work_item_status,
          event_type: :start
        )
      end
    end
  end
end
