# frozen_string_literal: true

require 'test_helper'

module App
  module WorkItems
    class WorkItemStatusesControllerTest < ActionDispatch::IntegrationTest
      let(:user) { create(:user) }
      let(:account) { create(:account, owner: user) }
      let(:project) { create(:project, account:) }
      let(:work_item) { create(:work_item, :backlog, project:) }

      before do
        all_item_statuses
        sign_in user
      end

      describe 'PATCH update' do
        it 'should redirect to the work item' do
          patch app_project_work_item_work_item_status_path(work_item.project, work_item), params: { transition_to: 'grooming' }

          assert_redirected_to app_project_work_item_path(work_item.project, work_item)
        end
      end
    end
  end
end
