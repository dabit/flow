# frozen_string_literal: true

class ApplicationPolicy
  attr_reader :user, :record

  def initialize(user, record)
    @user = user
    @record = record
  end

  def index?
    default_policy
  end

  def show?
    default_policy
  end

  def create?
    default_policy
  end

  def new?
    create?
  end

  def update?
    default_policy
  end

  def edit?
    update?
  end

  def destroy?
    default_policy
  end

  def default_policy
    false
  end

  class Scope
    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
      raise NotImplementedError, "You must define #resolve in #{self.class}"
    end

    private

    attr_reader :user, :scope
  end
end
