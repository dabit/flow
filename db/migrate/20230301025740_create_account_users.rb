class CreateAccountUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :account_users, id: :uuid do |t|
      t.belongs_to :user, null: false, foreign_key: true, type: :uuid
      t.belongs_to :account, null: false, foreign_key: true, type: :uuid
      t.integer :role, null: false, default: 0

      t.timestamps
    end
  end
end
