# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToWithdrawn < Services::WorkItems::Transitions::BaseTransition
      end
    end
  end
end
