class RenameWorkItemNameToTitle < ActiveRecord::Migration[7.0]
  def change
    rename_column :work_items, :name, :title
  end
end
