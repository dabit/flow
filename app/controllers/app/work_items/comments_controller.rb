# frozen_string_literal: true

module App
  module WorkItems
    class CommentsController < App::BaseController
      before_action :find_work_item

      def index
        @comments = @work_item.comments
      end

      def new
        @comment = @work_item.comments.new
      end

      def edit; end

      def create
        @comment = Services::Comments::Create.call(@work_item, comment_params)

        respond_to do |format|
          format.turbo_stream
        end
      end

      def destroy
        @comment = Current.user.comments.find(params[:id])
        @comment.destroy

        respond_to do |format|
          format.turbo_stream
        end
      end

      private

      def comment_params
        params.require(:comment).permit(:message)
      end
    end
  end
end
