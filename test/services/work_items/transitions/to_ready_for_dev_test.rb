# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    module Transitions
      class ToReadyForDevTest < ActiveSupport::TestCase
        let(:work_item) { create(:work_item, :backlog) }
        let(:user) { work_item.assigned_to }

        before do
          all_item_statuses
          Current.user = user
        end

        describe 'ready_for_dev' do
          before do
            Services::WorkItems::Transition.call(work_item, 'ready_for_dev')
          end

          it 'should transition the work item to ready_for_dev' do
            _(work_item.work_item_status).must_equal(create(:work_item_status, :ready_for_dev))
          end
        end
      end
    end
  end
end
