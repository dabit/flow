# frozen_string_literal: true

module Services
  module Projects
    class Create < ApplicationService
      def call(project_params)
        Current.account.projects.build(project_params).tap do |project|
          project.created_by = Current.user
          project.save
        end
      end
    end
  end
end
