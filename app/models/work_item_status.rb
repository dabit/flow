# frozen_string_literal: true

# == Schema Information
#
# Table name: work_item_statuses
#
#  id         :uuid             not null, primary key
#  backlog    :boolean          default(FALSE)
#  i18n_name  :string
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class WorkItemStatus < ApplicationRecord
  acts_as_list
  default_scope -> { order(:position) }

  def self.all_for_collection_select
    WorkItemStatus.all.map { |status| [status.id, status] }
  end

  def to_s
    I18n.t(i18n_name, scope: 'work_item_statuses')
  end

  def self.valid_work_item_statuses
    %w[backlog grooming ready_for_dev in_progress review done withdrawn fridge]
  end

  def method_missing(method, *args, &)
    return super unless method.to_s.end_with?('?')

    method.to_s.chop == i18n_name
  end

  def respond_to_missing?(method, *)
    method.to_s.end_with?('?') || super
  end
end
