# frozen_string_literal: true

# == Schema Information
#
# Table name: work_item_statuses
#
#  id         :uuid             not null, primary key
#  backlog    :boolean          default(FALSE)
#  i18n_name  :string
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :work_item_status do
    sequence :i18n_name, WorkItemStatus.valid_work_item_statuses.cycle

    to_create do |instance|
      instance.attributes = WorkItemStatus.find_or_create_by(i18n_name: instance.i18n_name).attributes
      instance.reload
    end

    WorkItemStatus.valid_work_item_statuses.each do |status|
      trait status do
        i18n_name { status }
      end
    end
  end
end
