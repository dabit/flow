# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToReview < Services::WorkItems::Transitions::BaseTransition
      end
    end
  end
end
