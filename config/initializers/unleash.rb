# frozen_string_literal: true

# rubocop:disable Naming/PredicateName, Style/OptionalBooleanParameter, Lint/MissingSuper
module Unleash
  class TestClient < Client
    def initialize
      Unleash.logger = Rails.logger
      Unleash.logger.level = Logger::DEBUG
    end

    def is_enabled?(feature, context = nil, _default_value_param = false)
      Unleash.logger.debug "Unleash::Client.is_enabled? feature: #{feature} with context #{context}"
      true
    end
    alias enabled? is_enabled?
  end
end
# rubocop:enable Naming/PredicateName, Style/OptionalBooleanParameter, Lint/MissingSuper

if Rails.env.production?
  Unleash.configure do |config|
    config.app_name = 'Flow'
    config.url = ENV.fetch('UNLEASH_URL', 'http://test.com')
    config.instance_id = ENV.fetch('UNLEASH_INSTANCE_ID', 'instance')
    config.logger = Rails.logger
  end
  UNLEASH = Unleash::Client.new
else
  UNLEASH = Unleash::TestClient.new
end
