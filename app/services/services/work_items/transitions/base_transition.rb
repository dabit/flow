# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class BaseTransition
        attr_accessor :target_status, :work_item

        def attributes
          { work_item_status: @target_status }
        end

        def call(work_item:, target:)
          @target_status = WorkItemStatus.find_by(i18n_name: target)
          @work_item = work_item

          Services::WorkItems::Update.call(work_item, attributes)
        end
      end
    end
  end
end
