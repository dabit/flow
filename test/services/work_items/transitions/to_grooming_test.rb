# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    module Transitions
      class ToGroomingTest < ActiveSupport::TestCase
        let(:work_item) { create(:work_item, :backlog) }
        let(:user) { work_item.assigned_to }

        before do
          all_item_statuses
          Current.user = user
        end

        describe 'grooming' do
          before do
            Services::WorkItems::Transition.call(work_item, 'grooming')
          end

          it 'should transition the work item to grooming' do
            _(work_item.work_item_status).must_equal(create(:work_item_status, :grooming))
          end
        end
      end
    end
  end
end
