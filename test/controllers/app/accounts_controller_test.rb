# frozen_string_literal: true

require 'test_helper'

module App
  class AccountsControllerTest < ActionDispatch::IntegrationTest
    let(:user) { create(:user) }

    before { sign_in user }

    describe 'POST #create' do
      it 'redirects to the account page' do
        post app_accounts_path, params: { account: { name: FFaker::Company.name } }

        assert_redirected_to app_account_url(Account.order(:created_at).last)
      end
    end

    describe 'GET #index' do
      it 'returns a success response' do
        get app_accounts_path

        assert_response :success
      end
    end

    describe 'GET new' do
      it 'returns a success response' do
        get new_app_account_path

        assert_response :success
      end
    end

    describe 'GET #show' do
      let(:account) { create(:account, owner: user, project_count: 20) }

      it 'returns a success response' do
        get app_account_url(account)

        assert_response :success
      end
    end

    describe 'GET #edit' do
      let(:account) { create(:account, owner: user) }

      it 'returns a success response' do
        get edit_app_account_url(account)

        assert_response :success
      end
    end

    describe 'PATCH #update' do
      let(:account) { create(:account, owner: user) }

      it 'redirects to the account page' do
        patch app_account_url(account), params: { account: { name: FFaker::Company.name } }

        assert_redirected_to app_account_url(account)
      end
    end
  end
end
