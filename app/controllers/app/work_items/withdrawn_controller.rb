# frozen_string_literal: true

module App
  module WorkItems
    class WithdrawnController < App::WorkItemsController
      def index
        @work_items = @project.work_items.includes(:work_item_status, assigned_to: :profile).withdrawn.page(params[:page])
      end
    end
  end
end
