import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="dropdown-navigator"
export default class extends Controller {
  connect() {

  }

  navigate(e) {
    Turbo.visit(e.target.selectedOptions[0].value)
  }
}
