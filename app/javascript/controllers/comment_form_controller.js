import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="comment-form"
export default class extends Controller {
  static targets = ['commentForm', 'commentFormContainer', 'newButtonContainer']

  connect() {
  }

  showForm() {
    this.commentFormContainerTarget.classList.toggle('hidden')
    this.newButtonContainerTarget.classList.toggle('hidden')
  }

  cancelComment() {
    this.commentFormContainerTarget.classList.toggle('hidden')
    this.newButtonContainerTarget.classList.toggle('hidden')
    this.commentFormTarget.reset()
  }
}
