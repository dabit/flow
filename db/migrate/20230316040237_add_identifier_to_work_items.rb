class AddIdentifierToWorkItems < ActiveRecord::Migration[7.0]
  def up
    add_column :work_items, :identifier, :string
    WorkItem.unscoped.all.each do |work_item|
      Services::WorkItems::Create.new.assign_identifier(work_item)
      work_item.save
    end
    change_column_null :work_items, :identifier, false
  end

  def down
    remove_column :work_items, :identifier
  end
end
