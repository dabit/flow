# frozen_string_literal: true

require 'test_helper'

module App
  class ProfilesControllerTest < ActionDispatch::IntegrationTest
    let(:user) { create(:user) }

    before { sign_in user }

    describe 'GET #edit' do
      it 'responds with success' do
        get edit_app_profile_path

        assert_response :success
      end
    end

    describe 'GET #show' do
      describe 'when user has profile profile' do
        before { create(:profile, user:) }

        it 'responds with success' do
          get app_profile_path

          assert_response :success
        end
      end

      it 'redirects to edit profile' do
        get app_profile_path

        assert_redirected_to edit_app_profile_path
      end
    end

    describe 'POST #create' do
      it 'redirects to profile' do
        post app_profile_path,
             params: {
               profile: {
                 first_name: FFaker::Name.first_name,
                 last_name: FFaker::Name.last_name,
                 time_zone: 'Eastern Time (US & Canada)'
               }
             }

        assert_redirected_to app_profile_path
      end
    end

    describe 'PATCH #update' do
      before { create(:profile, user:) }

      it 'redirects to profile' do
        patch app_profile_path,
              params: {
                profile: {
                  first_name: FFaker::Name.first_name,
                  last_name: FFaker::Name.last_name
                }
              }

        assert_redirected_to app_profile_path
      end
    end
  end
end
