# frozen_string_literal: true

module Services
  module Features
    class Create < ApplicationService
      def call(project, feature_params)
        @feature = project.features.build(feature_params).tap do |feature|
          feature.user = Current.user
          feature.save
        end
      end
    end
  end
end
