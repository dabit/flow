class RenameTimeToMarket < ActiveRecord::Migration[7.0]
  def change
    rename_column :work_item_data, :time_to_market_hours, :cycle_time_hours
    rename_column :work_item_data, :time_to_market_minutes, :cycle_time_minutes
  end
end
