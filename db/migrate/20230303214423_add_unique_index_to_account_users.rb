class AddUniqueIndexToAccountUsers < ActiveRecord::Migration[7.0]
  def change
    add_index :account_users, %i[user_id account_id], unique: true
  end
end
