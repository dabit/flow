# frozen_string_literal: true

require 'test_helper'

module App
  class FeaturesControllerTest < ActionDispatch::IntegrationTest
    let(:project) { create(:project) }
    let(:account) { project.account }
    let(:user) { account.owner }
    let(:feature) { create(:feature, project:) }

    before do
      sign_in user
      get app_account_path(account)
    end

    describe 'GET new' do
      it 'responds with success' do
        get new_app_project_feature_path(project)

        assert_response :success
      end
    end

    describe 'POST create' do
      it 'redirects to feature' do
        post app_project_features_path(project),
             params: {
               feature: {
                 name: FFaker::Lorem.sentence,
                 description: FFaker::Lorem.paragraph
               }
             }

        assert_redirected_to app_project_feature_path(project, Feature.last)
      end
    end

    describe 'GET show' do
      it 'responds with success' do
        get app_project_feature_path(project, feature)

        assert_response :success
      end
    end

    describe 'GET #index' do
      it 'responds with success' do
        create_list(:feature, 3, project:)
        get app_project_features_path(project)

        assert_response :success
      end
    end

    describe 'GET #edit' do
      it 'responds with success' do
        get edit_app_project_feature_path(project, feature)

        assert_response :success
      end
    end

    describe 'PATCH #update' do
      it 'redirects to feature' do
        patch app_project_feature_path(project, feature),
              params: {
                feature: {
                  name: FFaker::Lorem.sentence,
                  description: FFaker::Lorem.paragraph
                }
              }

        assert_redirected_to app_project_feature_path(project, feature)
      end
    end
  end
end
