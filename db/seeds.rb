work_item_statuses = {}
WorkItemStatus.valid_work_item_statuses.each do |status|
  work_item_statuses[status.to_sym] = create(:work_item_status, i18n_name: status)
end

user = create(:user, :with_profile, email: 'user@test.com')
account = create(:account, owner: user, project_count: 1)
project = account.projects.first

10.times do
  create(:account_user, user: create(:user, :with_profile), account:)
end

create_list(:work_item, 60, project:, created_by: user, assigned_to: user)

WorkItem.all.each do |work_item|
  create(:work_item_status_event, event_type: :start, work_item_status: work_item_statuses[:backlog],work_item: , user:, created_at: 2.weeks.ago)
end

WorkItem.done.each do |work_item|
  hours = rand(336)
  minutes = hours * 60
  create(:work_item_status_event, event_type: :end, work_item_status: work_item_statuses[:backlog], work_item: , user:, hours_spent: hours, minutes_spent: minutes)
  create(:work_item_status_event, event_type: :start, work_item_status: work_item_statuses[:done], work_item: , user:)

  create(:work_item_data, work_item: work_item, project: work_item.project)
end

WorkItem.fridge.update_all(stashed_in_fridge_at: Time.current)
