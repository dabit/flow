class CreateFeatures < ActiveRecord::Migration[7.0]
  def change
    create_table :features, id: :uuid do |t|
      t.string :name, null: false
      t.belongs_to :user, null: false, foreign_key: true, type: :uuid
      t.belongs_to :project, null: false, foreign_key: true, type: :uuid
      t.timestamp :deleted_at

      t.timestamps
    end

    add_index :features, :deleted_at
  end
end
