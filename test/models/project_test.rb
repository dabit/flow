# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id            :uuid             not null, primary key
#  deleted_at    :datetime
#  identifier    :string           not null
#  name          :string
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  account_id    :uuid
#  created_by_id :uuid
#
# Indexes
#
#  index_projects_on_account_id                 (account_id)
#  index_projects_on_created_by_id              (created_by_id)
#  index_projects_on_deleted_at                 (deleted_at)
#  index_projects_on_identifier_and_account_id  (identifier,account_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_...  (created_by_id => users.id)
#
require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
  let(:project) { create(:project) }

  describe 'averages' do
    before do
      create(:work_item_data, project:, cycle_time_hours: 24, cycle_time_minutes: 1440)
      create(:work_item_data, project:, cycle_time_hours: 48, cycle_time_minutes: 2880)
      create(:work_item_data, project:, cycle_time_hours: 99, cycle_time_minutes: 28_800,
                              updated_at: (Rails.configuration.flowu.average_threshold_weeks.weeks + 1.day).ago)
    end

    it 'returns the average cycle time in hours' do
      assert_equal 36, project.average_cycle_time_hours
    end

    it 'returns the average cycle time in minutes' do
      assert_equal 2160, project.average_cycle_time_minutes
    end
  end
end
