# frozen_string_literal: true

require 'test_helper'

module App
  class ProjectsControllerTest < ActionDispatch::IntegrationTest
    let(:user) { create(:user) }
    let(:account) { create(:account, owner: user) }
    let(:project) { create(:project, account:, created_by: user, work_item_count: 10) }

    before do
      sign_in user
      get app_account_path(account)
    end

    describe 'GET new' do
      it 'returns a success response' do
        get new_app_project_path

        assert_response :success
      end
    end

    describe 'POST create' do
      it 'redirects to the project page' do
        post app_projects_path, params: {
          project: {
            name: FFaker::Book.title,
            identifier: 'TEST'
          }
        }

        assert_redirected_to app_project_url(Project.order(:created_at).last)
      end
    end

    describe 'GET show' do
      it 'returns a success response' do
        get app_project_path(project)

        assert_response :success
      end
    end

    describe 'GET edit' do
      it 'returns a success response' do
        get edit_app_project_path(project)

        assert_response :success
      end
    end

    describe 'PATCH update' do
      it 'redirects to the project page' do
        patch app_project_path(project), params: {
          project: {
            name: FFaker::Book.title
          }
        }

        assert_redirected_to app_project_url(project)
      end
    end
  end
end
