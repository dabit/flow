import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="autodropdown"
export default class extends Controller {
  static targets = ['actionButton'];

  click() {
    this.actionButtonTarget.click();
  }
}
