# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToInProgress < Services::WorkItems::Transitions::BaseTransition
      end
    end
  end
end
