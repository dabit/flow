# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    class UpdateTest < ActiveSupport::TestCase
      let(:project) { create(:project) }
      let(:user) { project.created_by }

      before { Current.user = user }

      describe 'call' do
        let(:work_item) { create(:work_item, :backlog) }
        let(:last_event) { work_item.work_item_status_events.last }
        let(:target_status) { create(:work_item_status, :in_progress) }

        describe 'when status changes' do
          before do
            last_event
            Services::WorkItems::Update.call(
              work_item, {
                title: FFaker::Lorem.sentence,
                work_item_status_id: target_status.id
              }
            )
          end

          it 'creates an end event for last event' do
            _(work_item.work_item_status_events.where(work_item_status: last_event.work_item_status,
                                                      event_type: :end)).wont_be_empty
          end

          it 'creates a start event for new status' do
            _(work_item.work_item_status_events.where(work_item_status: target_status, event_type: :start)).wont_be_empty
          end
        end

        describe 'when status does not change' do
          before do
            last_event
            Services::WorkItems::Update.call(
              work_item, {
                title: FFaker::Lorem.sentence
              }
            )
          end

          it "won't create an end event for last event" do
            _(work_item.work_item_status_events.where(work_item_status: last_event.work_item_status,
                                                      event_type: :end)).must_be_empty
          end

          it "won't create a start event for new status" do
            _(work_item.work_item_status_events.where(work_item_status: target_status, event_type: :start)).must_be_empty
          end
        end
      end
    end
  end
end
