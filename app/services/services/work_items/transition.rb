# frozen_string_literal: true

module Services
  module WorkItems
    class Transition < ApplicationService
      def call(work_item, transition_to)
        "services/work_items/transitions/to_#{transition_to}".classify.constantize.new.call(target: transition_to, work_item:)
      end
    end
  end
end
