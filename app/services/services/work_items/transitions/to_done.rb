# frozen_string_literal: true

module Services
  module WorkItems
    module Transitions
      class ToDone < Services::WorkItems::Transitions::BaseTransition
        def call(work_item:, target:)
          return unless super

          Services::WorkItemData::Generate.call(work_item)
        end
      end
    end
  end
end
