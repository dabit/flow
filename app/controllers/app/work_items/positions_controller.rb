# frozen_string_literal: true

module App
  module WorkItems
    class PositionsController < App::BaseController
      def update
        @work_item = WorkItem.find(params[:work_item_id])
        @work_item.insert_at(params[:position].to_i)
      end
    end
  end
end
