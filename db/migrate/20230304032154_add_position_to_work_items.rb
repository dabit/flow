class AddPositionToWorkItems < ActiveRecord::Migration[7.0]
  def change
    add_column :work_items, :position, :integer, null: false, default: 1
    add_column :work_items, :deleted_at, :timestamp

    add_index :work_items, :position
    add_index :work_items, :deleted_at

    WorkItem.all.each_with_index do |work_item, index|
      work_item.update_column(:position, index + 1)
    end
  end
end
