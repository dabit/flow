import { Controller } from "@hotwired/stimulus"
import Sortable from 'sortablejs';
import { patch } from "@rails/request.js"

// Connects to data-controller="work-item-list"
export default class extends Controller {
  static targets = ['dragContainer']

  connect() {
    this.sortable= new Sortable(this.dragContainerTarget, {
      onEnd: this.insertAt,
      handle: '.handler',
      ghostClass: 'work-item-ghost',
      dragClass: 'work-item-drag'
    })
  }

  insertAt(e) {
    patch(e.item.dataset.updateUrl,
      { body: JSON.stringify({ position: e.newIndex + 1 }) }
    )
  }
}
