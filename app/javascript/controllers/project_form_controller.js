import { Controller } from "@hotwired/stimulus"
import { get } from "@rails/request.js"

// Connects to data-controller="project-form"
export default class extends Controller {
  connect() {
  }

  updateIdentifier(e) {
    clearTimeout(this.keyTimer)
    this.keyTimer = setTimeout(() => { get('/app/projects/new.turbo_stream?name=' + e.target.value) }, 1000)
  }
}
