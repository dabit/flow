# frozen_string_literal: true

require 'test_helper'

module Services
  module WorkItems
    module Transitions
      class ToWithdrawnTest < ActiveSupport::TestCase
        let(:work_item) { create(:work_item, :backlog) }
        let(:user) { work_item.assigned_to }

        before do
          all_item_statuses
          Current.user = user
        end

        describe 'withdrawn' do
          before do
            Services::WorkItems::Transition.call(work_item, 'withdrawn')
          end

          it 'should transition the work item to withdrawn' do
            _(work_item.work_item_status).must_equal(create(:work_item_status, :withdrawn))
          end
        end
      end
    end
  end
end
