namespace :after_party do
  desc 'Deployment task: generate_work_item_data'
  task generate_work_item_data: :environment do
    puts "Running deploy task 'generate_work_item_data'"

    WorkItem.done.includes(:work_item_status, :project, :work_item_status_events).all.each do |work_item|
      Services::WorkItemData::Generate.call(work_item)
    end

    # Update task as completed.  If you remove the line below, the task will
    # run with every deploy (or every time you call after_party:run).
    AfterParty::TaskRecord
      .create version: AfterParty::TaskRecorder.new(__FILE__).timestamp
  end
end
