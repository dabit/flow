# frozen_string_literal: true

module App
  class FeaturesController < App::BaseController
    before_action :find_project
    before_action :authorize_features

    def index
      @features = @project.features
    end

    def show
      @feature = @project.features.find(params[:id])
      @work_items = @feature.work_items.includes(:work_item_status, assigned_to: :profile)

      respond_with :app, @project, @feature
    end

    def new
      @feature = Feature.new
    end

    def edit
      @feature = @project.features.find(params[:id])

      respond_with :app, @project, @feature
    end

    def create
      @feature = Services::Features::Create.call(@project, feature_params)

      respond_with :app, @project, @feature
    end

    def update
      @feature = @project.features.find(params[:id])
      @feature.update(feature_params)

      respond_with :app, @project, @feature
    end

    def page_title
      I18n.t(action_name, scope: [:page_title, controller_name], account_name: Current.account.name, project_name: @project,
                          resource_name: @feature)
    end

    private

    def feature_params
      params.require(:feature).permit(:name, :description)
    end

    def authorize_features
      authorize Feature
    end
  end
end
