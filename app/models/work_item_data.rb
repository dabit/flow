# frozen_string_literal: true

# == Schema Information
#
# Table name: work_item_data
#
#  id                 :uuid             not null, primary key
#  cycle_time_hours   :integer
#  cycle_time_minutes :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  project_id         :uuid             not null
#  work_item_id       :uuid             not null
#
# Indexes
#
#  index_work_item_data_on_project_id    (project_id)
#  index_work_item_data_on_work_item_id  (work_item_id)
#
# Foreign Keys
#
#  fk_rails_...  (project_id => projects.id)
#  fk_rails_...  (work_item_id => work_items.id)
#
class WorkItemData < ApplicationRecord
  belongs_to :work_item
  belongs_to :project

  scope :for_average, -> { where(updated_at: Rails.configuration.flowu.average_threshold_weeks.weeks.ago..) }
end
