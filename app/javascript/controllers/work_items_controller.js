import { Controller } from "@hotwired/stimulus"

// Connects to data-controller="work-items"
export default class extends Controller {
  static targets = [ "descriptionSection", "commentsSection", "actionsSection" ]

  connect() {
  }

  showDescription(e) {
    this.hideAll()
    e.target.classList.add('section-selector-selected')
    this.descriptionSectionTarget.classList.remove('hidden')
  }

  showActions(e) {
    this.hideAll()
    e.target.classList.add('section-selector-selected')
    this.actionsSectionTarget.classList.remove('hidden')
  }

  showComments(e) {
    this.hideAll()
    e.target.classList.add('section-selector-selected')
    this.commentsSectionTarget.classList.remove('hidden')
  }

  hideAll() {
    this.descriptionSectionTarget.classList.add('hidden')
    this.commentsSectionTarget.classList.add('hidden')
    this.actionsSectionTarget.classList.add('hidden')
    Array.from(this.element.getElementsByClassName('section-selector')).forEach((el) => {
      el.classList.remove('section-selector-selected')
      el.classList.add('section-selector-default')
    })
  }
}
