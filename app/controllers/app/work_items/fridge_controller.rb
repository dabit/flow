# frozen_string_literal: true

module App
  module WorkItems
    class FridgeController < App::WorkItemsController
      def index
        @work_items = @project.work_items
                              .includes(:work_item_status, assigned_to: :profile)
                              .fridge
                              .order(stashed_in_fridge_at: :desc)
                              .page(params[:page])
      end
    end
  end
end
