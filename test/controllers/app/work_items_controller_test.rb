# frozen_string_literal: true

require 'test_helper'

module App
  class WorkItemsControllerTest < ActionDispatch::IntegrationTest
    let(:project) { create(:project) }
    let(:account) { project.account }
    let(:user) { account.owner }
    let(:work_item) { create(:work_item, project:) }

    before do
      sign_in user
      get app_account_path(account)
    end

    describe 'GET new' do
      it 'responds with success' do
        get new_app_project_work_item_path(project)

        assert_response :success
      end
    end

    describe 'POST create' do
      it 'redirects to work_item' do
        post app_project_work_items_path(project),
             params: {
               work_item: {
                 title: FFaker::Lorem.sentence,
                 work_item_type: WorkItem.work_item_types.keys.sample,
                 description: FFaker::Lorem.paragraph,
                 work_item_status_id: create(:work_item_status, :backlog).id
               }
             }

        assert_redirected_to app_project_work_item_path(project, WorkItem.last)
      end

      describe 'validation errors' do
        it 'responds with unprocessable entity' do
          post app_project_work_items_path(project),
               params: {
                 work_item: {
                   title: ''
                 }
               }

          assert_response :unprocessable_entity
        end
      end
    end

    describe 'GET show' do
      WorkItemStatus.valid_work_item_statuses.each do |work_item_status|
        describe "when the work item is in the #{work_item_status} status" do
          let(:work_item) { create(:work_item, work_item_status.to_sym, project:) }

          it 'responds with success' do
            get app_project_work_item_path(project, work_item)

            assert_response :success
          end
        end
      end
    end

    describe 'GET #index' do
      it 'responds with success' do
        create(:work_item, :backlog, project:)
        get app_project_work_items_path(project)

        assert_response :success
      end

      describe 'work items of each type exist' do
        before do
          WorkItemStatus.valid_work_item_statuses.each do |work_item_status|
            create(:work_item, work_item_status.to_sym, project:)
          end
        end

        it 'responds with success' do
          get app_project_work_items_path(project)

          assert_response :success
        end
      end
    end

    describe 'GET #edit' do
      it 'responds with success' do
        get edit_app_project_work_item_path(project, work_item)

        assert_response :success
      end
    end

    describe 'PATCH #update' do
      it 'redirects to work_item' do
        patch app_project_work_item_path(project, work_item),
              params: {
                work_item: {
                  title: FFaker::Lorem.sentence
                }
              }

        assert_redirected_to app_project_work_item_path(project, work_item)
      end

      describe 'validation errors' do
        it 'responds with unprocessable entity' do
          patch app_project_work_item_path(project, work_item),
                params: {
                  work_item: {
                    title: ''
                  }
                }

          assert_response :unprocessable_entity
        end
      end
    end
  end
end
